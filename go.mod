module gitlab.com/tanopanta/gitlab-group-cloner

go 1.13

require (
	github.com/go-yaml/yaml v2.1.0+incompatible
	github.com/xanzy/go-gitlab v0.28.0
	rsc.io/quote v1.5.2
)
