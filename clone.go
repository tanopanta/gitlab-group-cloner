package main

import (
	"fmt"

	"github.com/xanzy/go-gitlab"
)

func cloneGroupAll(groupID int, token string) {
	git := gitlab.NewClient(nil, token)

	opt := &gitlab.ListGroupProjectsOptions{
		IncludeSubgroups: gitlab.Bool(true),
	}

	projects, _, err := git.Groups.ListGroupProjects(groupID, opt)

	if err != nil {
		fmt.Printf("Groups.ListGroupProjects returned error")
	}

	fmt.Println(len(projects))
	for i := 0; i < len(projects); i++ {
		fmt.Println(projects[i].WebURL)
	}

}
