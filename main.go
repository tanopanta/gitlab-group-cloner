package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/go-yaml/yaml"
)

// Config Mapping
type Config struct {
	Gitlabtoken string
}

func main() {
	t := Config{}

	data, err := ioutil.ReadFile("configs/config.yaml")

	if err != nil {
		log.Fatalf("error: %v", err)
		os.Exit(1)
	}

	err = yaml.Unmarshal(data, &t)

	if err != nil {
		log.Fatalf("error: %v", err)
		os.Exit(1)
	}

	fmt.Println(t.Gitlabtoken)

	cloneGroupAll(7356959, t.Gitlabtoken)
}
